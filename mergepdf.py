#!/home/oscar/anaconda3/bin/python
import argparse
import os
import PyPDF2

""" This script merges all pdf-files in the current directory into one pdf-file.
    The new file name can be specified as argument to the script.
    Files are merged in alphabetical order.
"""

def merge(new_name):
    """Merges all .pdf files in cur work dir into new_name.pdf"""
    cwd = os.getcwd()
    file_names = [x for x in os.listdir(cwd) if x.split('.')[-1] == 'pdf']
    if len(file_names) == 0:
        print('No documents to merge in {}'.format(cwd))
        return
    with open(new_name, 'wb') as outfile:
        pdfMerger = PyPDF2.PdfFileMerger()
        print('Merging following PDF documents to {}:'.format(new_name))
        for file in sorted(file_names, key=str.lower):
            with open(file, 'rb') as infile:
                pdfFileObj = PyPDF2.PdfFileReader(infile)
                pdfMerger.append(pdfFileObj)
                print(infile.name)

        pdfMerger.write(outfile)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Merge all PDF documents located in the current working dir.')
    parser.add_argument("name", help="Name of the new PDF-file")
    args = parser.parse_args()

    merge(args.name + '.pdf')
