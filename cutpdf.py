#!/home/oscar/anaconda3/bin/python
import argparse
import PyPDF2
import os

""" This script cuts out a range of pages in a pdf-file and saves them in a new 
    pdf-file."""

def cut_pdf(file_name, start, end):
    """Creates a new pdf file from pages start-end in file_name.pdf."""
    if not os.path.exists(file_name):
        print('{} does not exist.'.format(file_name))
        return 
    new_name = '{filename}_{start}-{end}.pdf'.format(filename=file_name.replace('.pdf',''),start=start, end=end)
    with open(file_name, 'rb') as infile:
        pdfReader = PyPDF2.PdfFileReader(infile)
        if end > pdfReader.numPages:
            end = pdfReader.numPages
        pdfWriter = PyPDF2.PdfFileWriter()
        
        with open(new_name, 'wb') as outfile:
            for p_num in range(start-1, end):
                pdf_page = pdfReader.getPage(p_num)
                pdfWriter.addPage(pdf_page)
            pdfWriter.write(outfile)
        
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Makes a new PDF document \
                                    from file_name.pdf using the specified interval.')
    parser.add_argument("file_name", help="Name of the PDF.")
    parser.add_argument("start", type=int,help="Starting point, starts from page one.")
    parser.add_argument("end", type=int, help="Ending point, greater or equal to start.")
    args = parser.parse_args()
    start, end = args.start, args.end
    if end >= start and start > 0:
        cut_pdf(args.file_name, args.start, args.end)
    else:
        print("Wrong start or end.")

