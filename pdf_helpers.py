#!/usr/bin/python3
import os
import PyPDF2

def merge_pdfs(new_name):
    """Merges all pdf files in current working dir."""
    file_names = [x for x in os.listdir() if x.split('.')[-1] == 'pdf']
    with open(new_name, 'wb') as outfile:
        pdfMerger = PyPDF2.PdfFileMerger()
        for file_name in sorted(file_names):
            with open(file_name, 'rb') as infile:
                pdfFileObj = PyPDF2.PdfFileReader(infile)
                pdfMerger.append(pdfFileObj)
                print('{} appended'.format(infile.name))

        pdfMerger.write(outfile)
